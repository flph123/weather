<?php namespace Phantom\Weather\Base;

use Exception;
use Phantom\Weather\Libraries\JsonMapper;

abstract class Connector implements ConnectorInterface {


	/**
	 * Fetch data from web
	 *
	 * @param string $url
	 * @param mixed $response
	 * @param int $error
	 */
	protected function curl($url, &$response, &$error)
	{
		$handler = curl_init();
		curl_setopt($handler, CURLOPT_URL, $url);
		curl_setopt($handler, CURLOPT_RETURNTRANSFER, true);
		$response = curl_exec($handler);
		$error    = curl_errno($handler);
		curl_close($handler);
	}


	/**
	 * Returns an response
	 *
	 * @param string                                  $url
	 * @param array                                   $params
	 * @param \Phantom\Weather\Base\ResponseInterface $model
	 *
	 * @return ResponseInterface
	 * @throws \Exception
	 */
	protected function respond($url,Array $params, ResponseInterface $model){
		$response = $error = null;
		$url = $url.'?'.http_build_query($params);
		$this->curl($url, $response, $error);

		if($response){
			$json = json_decode($response, false);
			if(!isset($json->cod)){
				throw new Exception($response, 500);
			}
			elseif($json->cod == 200){
				$mapper = new JsonMapper();
				return $mapper->map($json, $model);
			}else{
				throw new Exception($json->message, $json->cod);
			}
		}

		if ($error) {
			$error_message = curl_strerror($error);
			throw new Exception($error_message, $error);
		}
	}



}