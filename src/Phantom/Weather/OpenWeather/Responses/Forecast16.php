<?php namespace Phantom\Weather\OpenWeather\Responses;

use Phantom\Weather\Base\ResponseInterface;

class Forecast16 implements ResponseInterface{

	/**
	 * @var \Phantom\Weather\OpenWeather\Entities\City
	 */
	public $city;

	/**
	 * Internal parameter
	 * @var integer
	 */
	public $cod;

	/**
	 * Internal parameter
	 * @var float
	 */
	public $message;

	/**
	 * Number of lines returned by this API call
	 * @var integer
	 */
	public $cnt;

	/**
	 * @var \Phantom\Weather\OpenWeather\Entities\Forecast16List[]
	 */
	public $list;
}