<?php namespace Phantom\Weather\OpenWeather\Entities;

class Wind{

	/**
	 * Wind speed. Unit Default: meter/sec, Metric: meter/sec, Imperial: miles/hour.
	 * @var float
	 */
	public $speed;

	/**
	 * Wind direction, degrees (meteorological)
	 * @var integer
	 */
	public $deg;

}