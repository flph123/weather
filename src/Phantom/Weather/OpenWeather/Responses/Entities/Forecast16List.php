<?php namespace Phantom\Weather\OpenWeather\Entities;

class Forecast16List{
	/**
	 * Time of data forecasted
	 * @var integer
	 */
	public $dt;

	/**
	 * @var Temp[]
	 */
	public $temp;

	/**
	 * Atmospheric pressure (on the sea level, if there is no sea_level or grnd_level data), hPa
	 * @var float
	 */
	public $pressure;

	/**
	 * Humidity, %
	 * @var int
	 */
	public $humidity;

	/**
	 * More info Weather into condition codes
	 * @var Weather[]
	 */
	public $weather;

	/**
	 * Wind speed. Unit Default: meter/sec, Metric: meter/sec, Imperial: miles/hour.
	 * @var float
	 */
	public $speed;

	/**
	 * Wind direction, degrees (meteorological)
	 * @var integer
	 */
	public $deg;

	/**
	 * Cloudiness, %
	 * @var int
	 */
	public $clouds;

}