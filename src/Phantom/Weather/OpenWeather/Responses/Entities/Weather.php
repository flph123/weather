<?php namespace Phantom\Weather\OpenWeather\Entities;

class Weather{

	/**
	 * Weather condition id
	 * @var integer
	 */
	public $id;

	/**
	 * Group of weather parameters (Rain, Snow, Extreme etc.)
	 * @var string
	 */
	public $main;

	/**
	 * Weather condition within the group
	 * @var string
	 */
	public $description;

	/**
	 * Weather icon id
	 * @var string
	 */
	public $icon;

}