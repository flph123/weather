<?php namespace Phantom\Weather\OpenWeather\Entities;

class Rain{
	/**
	 * Rain volume for the last 3 hours
	 * @var integer
	 */
	public $_3h;

	/**
	 * @param int $val
	 */
	public function set3h($val){ $this->_3h = $val;}
}