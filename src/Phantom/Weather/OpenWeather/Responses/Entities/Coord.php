<?php namespace Phantom\Weather\OpenWeather\Entities;

class Coord{
	/**
	 * City geo location, longitude
	 * @var float
	 */
	public $lon;

	/**
	 * City geo location, latitude
	 * @var float
	 */
	public $lat;

}