<?php namespace Phantom\Weather\OpenWeather\Entities;

class Main{
	/**
	 * Temperature. Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
	 * @var float
	 */
	public $temp;

	/**
	 * Atmospheric pressure (on the sea level, if there is no sea_level or grnd_level data), hPa
	 * @var float
	 */
	public $pressure;

	/**
	 * Humidity, %
	 * @var int
	 */
	public $humidity;

	/**
	 * Minimum temperature at the moment.
	 * This is deviation from current temp that is possible for large cities and megalopolises geographically expanded (use these parameter optionally).
	 * Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
	 * @var float
	 */
	public $temp_min;

	/**
	 * Maximum temperature at the moment.
	 * This is deviation from current temp that is possible for large cities and megalopolises geographically expanded (use these parameter optionally).
	 * Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
	 * @var float
	 */
	public $temp_max;

	/**
	 * Atmospheric pressure on the sea level, hPa
	 * @var float
	 */
	public $sea_level;

	/**
	 * Atmospheric pressure on the ground level, hPa
	 * @var float
	 */
	public $grnd_level;
}