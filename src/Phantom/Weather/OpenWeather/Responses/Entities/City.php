<?php namespace Phantom\Weather\OpenWeather\Entities;

class City{

	/**
	 * City ID
	 * @var integer
	 */
	public $id;

	/**
	 * City name
	 * @var string
	 */
	public $name;

	/**
	 * @var Coord
	 */
	public $coord;

	/**
	 * Country code (GB, JP etc.)
	 * @var string
	 */
	public $country;

	/**
	 * @var int
	 */
	public $population;
}