<?php namespace Phantom\Weather\OpenWeather\Entities;

class Temp{
	/**
	 * Day temperature at the moment.
	 * This is deviation from current temp that is possible for large cities and megalopolises geographically expanded (use these parameter optionally).
	 * Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
	 * @var float
	 */
	public $day;

	/**
	 * Minimum temperature at the moment.
	 * This is deviation from current temp that is possible for large cities and megalopolises geographically expanded (use these parameter optionally).
	 * Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
	 * @var float
	 */
	public $min;

	/**
	 * Maximum temperature at the moment.
	 * This is deviation from current temp that is possible for large cities and megalopolises geographically expanded (use these parameter optionally).
	 * Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
	 * @var float
	 */
	public $max;

	/**
	 * Night temperature at the moment.
	 * This is deviation from current temp that is possible for large cities and megalopolises geographically expanded (use these parameter optionally).
	 * Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
	 * @var float
	 */
	public $night;

	/**
	 * Evening temperature at the moment.
	 * This is deviation from current temp that is possible for large cities and megalopolises geographically expanded (use these parameter optionally).
	 * Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
	 * @var float
	 */
	public $eve;

	/**
	 * Morning temperature at the moment.
	 * This is deviation from current temp that is possible for large cities and megalopolises geographically expanded (use these parameter optionally).
	 * Unit Default: Kelvin, Metric: Celsius, Imperial: Fahrenheit.
	 * @var float
	 */
	public $morn;

}