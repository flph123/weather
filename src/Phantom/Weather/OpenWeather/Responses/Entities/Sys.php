<?php namespace Phantom\Weather\OpenWeather\Entities;

class Sys{

	/**
	 * Internal parameter
	 * @var float
	 */
	public $message;

	/**
	 * Country code (GB, JP etc.)
	 * @var string
	 */
	public $country;

	/**
	 * Sunrise time, unix, UTC
	 * @var int
	 */
	public $sunrise;

	/**
	 * Sunset time, unix, UTC
	 * @var int
	 */
	public $sunset;

}