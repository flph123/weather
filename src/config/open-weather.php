<?php

return [
		#Config taken from http://openweathermap.org/api
		'base_path' => 'http://api.openweathermap.org/data/2.5',
		'app_id' => '',
		'search_type' => 'accurate',
		'units' => 'metric'
];